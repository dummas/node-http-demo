const http2 = require('http2');
const fs = require('fs');

const server = http2.createSecureServer({
  key: fs.readFileSync('../ssl/localhost-key.pem'),
  cert: fs.readFileSync('../ssl/localhost-cert.pem')
});

server.listen(8443, "localhost");

(async () => {
  while (true) {
    console.log('Listening...');
    /** @type {http2.ServerHttp2Stream} */
    const stream = await new Promise((resolve) => server.on('stream', (stream, headers) => {
      console.log('Stream id', stream.id);
      console.log('Method', headers[':method']);
      console.log('Path', headers[':path']);
      resolve(stream);
    }));

    const html = fs.readFileSync('../public/index.html');

    stream.respond({
      "content-type": 'text/html; charset=utf-8',
      ':status': 200
    });

    stream.write(html)
    stream.end();
  }
})();


const http2 = require('http2');
const fs = require('fs');
const client = http2.connect('https://localhost:8443', {
  ca: fs.readFileSync('localhost-cert.pem'),
});

(async () => {
  const request = client.request({ ":path": "/" });

  const response = await new Promise((resolve) => {
    let data = '';
    request.on('data', (chunk) => { 
      console.log('chunk', chunk);
      data += chunk 
    });
    request.on('end', () => {
      resolve(data);
    });
  });

  console.log('response', response);

  client.close();
})();

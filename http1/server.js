const net = require('net');
const fs = require('fs');

const server = net.createServer();
server.listen(8080, "localhost");

(async () => {
  while (true) {  
    console.log("Listening...");
    const socket = await new Promise((resolve) => server.on('connection', (socket) => resolve(socket)));
    const request = await new Promise((resolve) => socket.on('data', (data) => resolve(data.toString())));
    console.log('Request', request);

    const html = fs.readFileSync('../public/index.html');

    socket.write("HTTP/1.1 200 OK\r\n");
    socket.write("Content-Type: text/html; encoding=utf8\r\n")
    socket.write(`Content-Length: ${html.length}\r\n`)
    socket.write("Connection: close\r\n")
    socket.write("\r\n")
    socket.write(html);
    socket.end();
  }
  
})();

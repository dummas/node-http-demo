const net = require('net');

(async () => {
  const socket = new net.Socket();
  socket.connect(8080, "localhost");
  socket.write("GET / HTTP/1.1\r\n");
  
  const data = await new Promise((resolve) => {
    socket.on('data', (data) => resolve(data.toString()));
  });
  console.log('response', data);
  
  socket.end();
})();
